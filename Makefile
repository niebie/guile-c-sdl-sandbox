# i think this can be made more reliable.
SRC=./src
SCM=./scm
OUT=./out
INC=./include
CC=gcc

# game vars
GAME_FILES=\
	$(SRC)/main_menu.c\
	$(SRC)/game.c\
	$(SRC)/main.c
GAME_OUT_NAME=$(OUT)/game
GAME_INC_FLAGS=-I$(INC)

# guile vars
GUILE_TEST_FILES=$(SRC)/guile_load_test.c
GUILE_TEST_OUT_NAME=$(OUT)/guile-test
GUILE_TEST_INC_FLAGS=-I$(INC)

SDL_LIB_FLAGS=-lSDL2 -lSDL2_ttf
GUILE_LIB_FLAGS=-L/run/current-system/profile/lib -lguile-2.2
GUILE_INC_FLAGS=-I/run/current-system/profile/include/guile/2.2 -I/run/current-system/profile/include
DEBUG_FLAGS=-DDEBUG

all:clean mkdirs game guile-test run-test

game:$(GAME_FILES)
	@echo CC game
	@$(CC)\
		$(GAME_INC_FLAGS)\
		$(GAME_FILES)\
		$(SDL_LIB_FLAGS)\
		$(DEBUG_FLAGS)\
		-o $(GAME_OUT_NAME)

guile-test:$(GUILE_TEST_FILES)
	@echo CC guile-test
	@$(CC)\
		$(GUILE_TEST_FILES)\
		$(GUILE_TEST_INC_FLAGS)\
		$(GUILE_LIB_FLAGS)\
		$(GUILE_INC_FLAGS)\
		$(DEBUG_FLAGS)\
		-o $(GUILE_TEST_OUT_NAME)

run-test:guile-test
	@echo
	@echo '*** RUNNING TEST ***'
	@echo
	@$(OUT)/guile-test

.PHONY:all clean

clean:
	@rm -f guile-test
	@rm -f game
	@rm -f *.o

mkdirs:
	@mkdir -p $(OUT)
