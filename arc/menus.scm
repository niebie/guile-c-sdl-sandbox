;; -*- geiser-scheme-implementation: guile -*-

;;; this file is a mockup of menus structure.

(use-modules (srfi srfi-9))             ;records
(use-modules (srfi srfi-28))            ;string format

(define-record-type <menu-tree-type>
  (make-menu-tree name text items)
  menu-tree-type?
  (name menu-name)
  (text menu-text)
  (items menu-items))

(define-record-type <menu-button-type>
  (make-menu-button-type name text)
  menu-button-type?
  (name button-name)
  (text button-text))

(define make-dumb-list
  (lambda (some-list)
    (begin
      some-list)))

(define make-settings-menu
  (make-menu-tree 'settings-menu
                  "settings"
                  (make-dumb-list (list "screen size"
                                        "fps counter"
                                        "key bindings"))))



(define screen-transition
  (lambda (key)
    ;; we are going to transition the active string to the registered
    ;; screen given by key.
    (if (eq? (lookup-screen key) #f)
        (begin
          (display "fucking no screen by that key bitch"))
        (begin
          (display "found the fucking screen, switching to it.")))
    ))

(define make-list-box
  (lambda ()))

(define get-x
  (lambda (pair)
    (car pair)))

(define get-y
  (lambda (pair)
    (cdr pair)))

(define screen-size-options
  (list
   ;; ((x . y) . "display value");
   '((1280 . 720) . "1280x720")     ; hidef
   '((800 . 600) . "800x600")
   '((640 . 480) . "640x480")
   )
  )

(define make-settings-menu
  (lambda ()
    (begin
      ;; screen size
      (make-list-box screen-size-options)
      ;; fps toggle
      ;; keybindings
      ;; back
      )))

(define main-menu-tree
  (list (make-option "start"
                     (screen-transition "game"))
        (make-option "settings"
                     (make-settings-menu))
        (make-option "quit"
                     (game-quit))))

(define main-menu
  (make-menu-tree
    'main-menu "main menu"
    (list (make-menu-button-type
           'start-button
           "start")
          (make-menu-button-type
           'settings-button
           "settings")
          (make-menu-button-type
           'quit-button
           "quit"))))

(define render-menu
  (lambda (menu)
    (if (menu-tree-type? menu)
        (begin
          (display (format "[~a]:'~a'"
                            (menu-name menu)
                            (menu-text menu)))
           (newline)
           (render-menu (menu-items menu)))
        (display (format "fucking not a menu:\n~a"
                         menu)))
    (newline)
    ))

(define-public entry
  (lambda ()
    (display "walking the menus")
    (newline)
    (render-menu main-menu)
    (render-menu "not a fucking menu")
    ))


;;; main menu
;;; -start
;;; \-switch to main game screen?
;;; -settings
;;; \-screen size
;;;  -fps enable
;;;  -key bindings
;;; -quit
;;;  (bound to exit)
