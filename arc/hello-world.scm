(use-modules (sdl2)
             (sdl2 render)
             (sdl2 surface)
             (sdl2 video))

(define (draw ren)
  (let* ((surface (load-bmp "hello.bmp"))
         (texture (surface->texture ren surface)))
    (clear-renderer ren)
    (render-copy ren texture)
    (present-renderer ren)
    (sleep 2)))

(define stop #f)
(define update-counter 0)

(define (update renderer)
  "update function, tail recursive."

  ;; this code is a test of killing the prog on stop.
  ;; (set! update-counter (1+ update-counter))
  ;; (if (> update-counter 10000)
  ;; 	  (set! stop #t))

  (clear-renderer renderer)

  
  
  (present-renderer renderer)
  
  ;; only run the updater if we are not stopping
  (if (not stop) (update renderer)))

(sdl-init)

(call-with-window (make-window)
  (lambda (window)
    (call-with-renderer (make-renderer window) update)))

(sdl-quit)
