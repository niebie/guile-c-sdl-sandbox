;;; this file is a c interface to the records defined in types.scm,
;;; since direct access of the auto created getters is not working
;;; from c for some reason ( Wrong type to apply: #<syntax-transformer
;;; getter> ) 

(define-module (niebie game c-interface)
  #:use-module (niebie game types)
  #:export (get-state-name
            get-state-type
            get-state-bg
            get-state-options
                           
            get-option-text
            get-option-type
            get-option-selections
            get-option-action
            ))

(define get-state-name
  (lambda (state)
    (state-name state)))

(define get-state-type
  (lambda (state)
    (state-type state)))

(define get-state-bg
  (lambda (state)
    (state-bg state)))

(define get-state-options
  (lambda (state)
    (state-options state)))

(define get-option-text
  (lambda (state)
    (option-text state)))

(define get-option-type
  (lambda (state)
    (option-type state)))

(define get-option-selections
  (lambda (state)
    (option-selections state)))

(define get-option-action
  (lambda (state)
    (option-action state)))
