;; -*- geiser-scheme-implementation: guile -*-

;;; this file is part of the sdl/opengl/guile game implemented
;;; by balzer inc.

;;; intent;
;;;  this file defines actions that are available for use, such as
;;;  state transitions and quitting the game.
;;; 

(define-module (niebie game actions)
  #:export (change-resolution
            change-state
            exit-game))

(define change-resolution
  (lambda (res)
    (display (format #f "changing the res to ~a" res))
    (newline)))

(define change-state
  (lambda (a)
    (display (format #f "change-state to ~a" a))
    (newline)))

(define exit-game
  (lambda ()
    (display "totally exiting the game i swear")
    (newline)))
