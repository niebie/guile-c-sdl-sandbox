;; -*- geiser-scheme-implementation: guile -*-

;;; this file is part of the sdl/opengl/guile game implemented
;;; by balzer inc.

;;; intent:
;;;  this file is meant to be a store of information for various art
;;;  files that are needed for the game.
;;;

(define-module (niebie game artwork)
  ;; #:use-module (niebie game ...)
  #:export (main-menu-bg))

(define-public default-bg "")

(define-public main-menu-bg default-bg)
(define-public game-bg default-bg)
(define-public settings-bg default-bg)
