;; -*- geiser-scheme-implementation: guile -*-

;;; this file is part of the sdl/opengl/guile game implemented
;;; by balzer inc.

;;; intent:
;;;  this file is effectively a configuration file that lays out the
;;;  various states and the transitions between them
;;;

;;; this will allow modules to be autoloaded.
(add-to-load-path "/home/niebie/sc/sdl/scm/modules")

(define-module (niebie game states)
  #:use-module (niebie game artwork)
  #:use-module (niebie game types)
  #:use-module (niebie game actions)
  #:export (states
            initial-state))

(define game-state
  (create-new-state
   #:name "game"
   #:type (state-types game)
   #:bg game-bg
   ;; #:bindings
   ;;    (list
   ;;     (create-binding
   ;;      #:name "move up"
   ;;      #:sprite main-character))
   ))

(define main-menu-state
  (create-new-state
   #:name "main-menu"
   #:type (state-types menu)
   #:bg main-menu-bg
   #:options
   (list
    (create-new-option
     #:text "start game"
     #:type (option-types button)
     #:action '(change-state (real-states game)))
    (create-new-option
     #:text "settings"
     #:type (option-types button)
     #:action '(change-state (real-states settings)))
    (create-new-option
     #:text "quit game"
     #:type (option-types button)
     #:action '(exit-game)))))

(define settings-state
  (create-new-state
   #:name "settings"
   #:type (state-types menu)
   #:bg settings-bg
   #:options
   (list
    (create-new-option
     #:text "toggle fps counter")
    (create-new-option
     #:text "select resolution"
     #:type (option-types selection-list)
     #:selections (list '((640 . 480) . "640 by 480")
                        '((800 . 600) . "800 by 600")
                        '((1280 . 720) . "1280 by 720"))
     #:action '(change-resolution)
     )
    (create-new-option
     #:text "back"
     #:type (option-types button)
     #:action '(change-state (real-states main-menu)
                             ;; one day this is selectable
                             ;; from the pause menu.
                             )))))

(define states
  (list main-menu-state
        settings-state
        game-state))

(define initial-state
  settings-state)

(define fps-counter
  #t)

(define screen-resolution
  '(640 . 480))
