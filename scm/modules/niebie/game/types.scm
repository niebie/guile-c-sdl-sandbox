;; -*- geiser-scheme-implementation: guile -*-

;;; this file is part of the sdl/opengl/guile game implemented
;;; by balzer inc.

;;; intent;
;;;  this file defines some types that are useful and generic to be
;;;  used across the game from all sorts of files.

(define-module (niebie game types)
  #:use-module (niebie game artwork)
  #:use-module (srfi srfi-9)
  #:export (;; macro'd helper functions
            create-new-state
            create-new-option
            ;; real types
            state-type
            option-type
            ;; enum types
            state-types
            option-types
            real-states
            ;; type accessor functions
            state-name state-type state-bg state-options
            option-text option-type option-selections option-action
            )
  )

(use-modules ((rnrs enums) :version (6)))

;;; states
(define-enumeration
  state-types
  (menu game undefined)
  state-types-constructor)

(define-enumeration
  real-states
  (main-menu
   game
   settings)
  real-states-constructor)

(define-record-type <state-type>
  (create-state name type bg options)
  state?
  (name state-name)
  (type state-type)
  (bg state-bg)
  (options state-options)
  )

;;; this has built in defaults for the keys
(define* (create-new-state #:key
                       (name "")
                       (type (state-types undefined))
                       (bg default-bg)
                       (options '()))
  (create-state name type bg options))

;;; options
(define-enumeration
  option-types
  (button selection-list undefined)
  option-types-constructor)

(define-record-type <option-type>
  (create-option text type selections action)
  state?
  (text option-text)
  (type option-type)
  (selections option-selections)
  (action option-action)
  )

(define* (create-new-option #:key
                        (text "")
                        (type (option-types undefined))
                        (selections #f)
                        (action #f))
  (create-option text type selections action))
