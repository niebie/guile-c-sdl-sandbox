struct game_t{
  SDL_Window   *window;
  SDL_Renderer *renderer;
  TTF_Font     *font;

  void (*update)(void);
  void (*render)(void);
  void (*cleanup)(void);
  void (*loop)(void);
  struct screen_state_t *states;
  int cur_state, state_count;
};

struct game_t *init_game(void);
