#ifndef _h_screen_state_
#define _h_screen_state_

#include <SDL2/SDL.h>

struct screen_state_t{
  /* debugging name */
  char *name;
  /* the main texture that gets copied at render time. */
  SDL_Texture *texture;
  struct action_t *actions;
  
  void (*update)(void);
  void (*render)(void);
  void (*cleanup)(void);
  void (*initializer)(struct screen_state_t *state,
                      SDL_Renderer *renderer);
};
#endif
