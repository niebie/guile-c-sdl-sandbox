#ifndef _h_config_
#define _h_config_

#include "screen_state.h"
#include "screen_resolution.h"

struct config_t{
  /* bools */
  char valid, fps_counter;
  struct screen_state_t *initial_state;
  struct screen_state_t *states;
  struct screen_resolution_t *res;
};

#endif
