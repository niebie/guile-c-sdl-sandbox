#ifdef DEBUG
#define pdebug(...) {printf("[DEBUG]: "); printf(__VA_ARGS__); printf("\n");}
#else
#define pdebug(...) { }
#endif

#define _perror(...) {                      \
    fprintf(stderr, "[ERROR]: ");          \
    fprintf(stderr,__VA_ARGS__);           \
    fprintf(stderr, "\n");  }

#define verbose_error_exit(...){_perror(__VA_ARGS__); exit(EXIT_FAILURE);}
