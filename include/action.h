
struct action_t{
  char        *name;            /* debugging */
  SDL_Keycode  key;             /* key */
  struct screen_state_t* (*action)(void); /* action to take */
};
