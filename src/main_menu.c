/* standard libs */
#include <stdlib.h>

/* library libs */
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/* local ../include/* */
#include <macros.h>
#include <main_menu.h>
#include <action.h>

#define FONT_LOC "/home/niebie/sc/telegram-build/tdesktop/Telegram/Resources/fonts/OpenSans-Regular.ttf"
#define menu_len() (sizeof(items) / sizeof(struct menu_item_t))

static struct screen_state_t *main_menu = NULL;
struct screen_state_t *screen_swap_request = NULL;

enum color_t{
  WHITE, RED
};

#define X(c, r, g, b) [c] = { r, g, b }
static SDL_Color colors[] = {
  X(WHITE, 255, 255, 255),
  X(RED, 255, 0, 0)
};
#undef X

static struct screen_vars_t{
  SDL_Renderer *renderer;
  TTF_Font     *font;
  int           menu_item_height;
  int           menu_item_gap;
  int           menu_item_left;
  int           menu_item_top;
  int           menu_item_selected;
} vars;

enum options { START, SETTINGS, QUIT };

#define X(name, text, ac, ic)                           \
  [name] = {                                            \
      text,                                             \
      ac, ic,                                           \
      NULL, NULL, NULL, NULL,                           \
      { 0, 0, 0, 0 }                                    \
  }

struct menu_item_t {
  char        *text;
  enum color_t act_color, inact_color;
  SDL_Texture *act_texture, *inact_texture;
  SDL_Surface *act_surface, *inact_surface;
  SDL_Rect     pos;
};

static struct menu_item_t items[] = {
  X(START, "start", RED, WHITE),
  X(SETTINGS, "settings", RED, WHITE),
  X(QUIT, "quit", RED, WHITE)
};
#undef X

struct screen_state_t *menu_handler(void){
  pdebug("current selection [%d]:'%s'",
         vars.menu_item_selected,
         items[vars.menu_item_selected].text);
  switch(vars.menu_item_selected){
  case QUIT:
    pdebug("quitting as requested");
    exit(EXIT_SUCCESS);
    break;
  case START:
    pdebug("swapping to start state");
    break;
  case SETTINGS:
    pdebug("swapping to settings state");
    break;
  default:
    pdebug("unknown option.");
    break;
  }
  return NULL;
}

struct screen_state_t *inc_selected(void){
  int count = menu_len();
  vars.menu_item_selected++;
  if(vars.menu_item_selected >= count){
    vars.menu_item_selected = 0;
  }
  return NULL;
}

struct screen_state_t *dec_selected(void){
  int count = menu_len();
  vars.menu_item_selected--;
  if(vars.menu_item_selected < 0){
    vars.menu_item_selected = count - 1;
  }
  return NULL;
}

#define X(name, key, action_handler) { name, key, action_handler }
static struct action_t actions[] = {
  X("menu handler", SDLK_RETURN, menu_handler),
  X("next menu item", SDLK_n, inc_selected),
  X("next menu item", SDLK_p, dec_selected)
};
#undef X

/* needs to be on a state-by-state basis */
static void init_font(){
  pdebug("begin init font");
  char *font_txt = malloc(sizeof(char) * 100);
  strcpy(font_txt,
         FONT_LOC);
  
  pdebug("opening font file %s", font_txt);
  vars.font = 
    TTF_OpenFont(font_txt,
                 13);
  
  if (vars.font == NULL) {
    perror("error: font not found\n");
    printf("ttf failed: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  } 
  pdebug("end init font");
}

static void setup_menu_text(){
  /* pdebug("start menu text setup"); */

  int count = menu_len();

  for(int i = 0;
      i < count;
      i++){

    if(items[i].act_surface){
      SDL_FreeSurface(items[i].act_surface);
    }
    if(items[i].inact_surface){
      SDL_FreeSurface(items[i].inact_surface);
    }
    
    items[i].act_surface =
      TTF_RenderText_Solid(vars.font,
                           items[i].text,
                           colors[items[i].act_color]);
    items[i].inact_surface =
      TTF_RenderText_Solid(vars.font,
                           items[i].text,
                           colors[items[i].inact_color]);

    
    if(items[i].act_texture){
      SDL_DestroyTexture(items[i].act_texture);
    }
    if(items[i].inact_texture){
      SDL_DestroyTexture(items[i].inact_texture);
    }

    items[i].act_texture =
      SDL_CreateTextureFromSurface(vars.renderer,
                                   items[i].act_surface);
    
    items[i].inact_texture =
      SDL_CreateTextureFromSurface(vars.renderer,
                                   items[i].inact_surface);
  }
  /* pdebug("end menu text setup"); */
}

void handle_key_up(SDL_Event event){
  switch(event.key.keysym.sym){
  default:
    /* iterate through actions to find a handler */
    {
      int count = (sizeof(actions) / sizeof(struct action_t));
      for(int i = 0;
          i < count;
          i++){
        if(event.key.keysym.sym == actions[i].key){
          struct screen_state_t *swap = actions[i].action();
          if(swap){
            screen_swap_request = swap;
          } else{
            screen_swap_request = NULL;
          }
        }
      }
    }
    break;
  }
}

void update(void){
  /* pdebug("main menu updater"); */

  SDL_Event event;
  /* check for key presses */
  if(SDL_WaitEvent(&event) != 0){
    //while(SDL_PollEvent(&event)){
    switch(event.type){
    case SDL_KEYDOWN:
      pdebug("key down [%s]",
             SDL_GetKeyName(event.key.keysym.sym));
      break;
    case SDL_KEYUP:
      pdebug("key up   [%s]",
             SDL_GetKeyName(event.key.keysym.sym));
      handle_key_up(event);
      break;
    case SDL_QUIT:
      exit(EXIT_SUCCESS);
    default:
      
      break;
    }
  }
}

void render_menu_items(void){
  int count = menu_len();
  /* pdebug("menu item count:%u", count); */

  vars.menu_item_gap = 5;
  vars.menu_item_top = 0;
  vars.menu_item_left = 10;
  vars.menu_item_height = 0;
  for(int i = 0;
      i < count;
      i++){
    SDL_QueryTexture(i == vars.menu_item_selected ?
                     items[i].act_texture :
                     items[i].inact_texture,
                     NULL, NULL,
                     &items[i].pos.w, &items[i].pos.h);
    /* double sizes b/c we want a jagged text */
    items[i].pos.w *= 3;
    items[i].pos.h *= 3;
    items[i].pos.x = vars.menu_item_left;
    items[i].pos.y =
        vars.menu_item_top
      + vars.menu_item_height;
    vars.menu_item_height = items[i].pos.h;
    vars.menu_item_top = items[i].pos.y;

    SDL_RenderCopy(vars.renderer,
                   i == vars.menu_item_selected ?
                   items[i].act_texture :
                   items[i].inact_texture,
                   NULL,
                   &items[i].pos);
    /* pdebug("rendered item %d [%s]", i, items[i].text); */
  }
}

void render(void){
  /* pdebug("main menu renderer"); */
  render_menu_items();
}

void cleanup(void){
  /* todo */
}

void main_menu_init(struct screen_state_t *state,
                    SDL_Renderer *renderer){
  main_menu = state;
  main_menu->texture = NULL;
  main_menu->update = update;
  main_menu->render = render;
  main_menu->cleanup = cleanup;
  vars.renderer = renderer;

  init_font();
  vars.menu_item_selected = 0;
  setup_menu_text();
  state->actions = actions;
}
