#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/* local ../include/* */
#include <macros.h>
#include <game.h>

int main(int argc, char *argv[]) {

  struct game_t *game = init_game();
  game->loop();
  game->cleanup();
  
  return EXIT_SUCCESS;
}
