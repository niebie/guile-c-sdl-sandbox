#include <libguile.h>
#include <stdlib.h>

/* local ../include/* */
#include <macros.h>
#include <config.h>

#define CONFIG_FILE "./game-config.scm"
#define root "/home/niebie/sc/sdl/scm/modules/niebie/game/"
#define SCM_STR(str) (scm_from_locale_string(str))

void *some_func(void *arg){
  /* this code will get the scm value for a given string variable so
     that we can call scheme functions with it directly.*/
  /* SCM file_string = 
       scm_from_locale_string(file); */
  
  SCM res =
    scm_c_primitive_load(root "states.scm");
  SCM res1 =
    scm_c_primitive_load(root "c-interface.scm");

  SCM states =
    scm_c_public_ref("niebie game states", "states");
  /* this code gets a value for the variable, not the variable's
     value like we need. */
  /* scm_c_public_variable("niebie game states", "states"); */    

  if(scm_is_false(states)){
    pdebug("states not defined in module (niebie game states)");
    goto fail;
  }

  pdebug("states was found.");

  if(scm_is_false(scm_list_p(states))){
    pdebug("states isn't a list.");
    goto fail;
  }

  pdebug("states is defined as a list.");

  /* now we need to iterate over it to figure out what values were
     configured in the states module. */

  /* order doesn't matter */
  SCM len = scm_length(states);
  int c_len = scm_to_int(len);
  int i;
  for(i = c_len - 1;
      i >= 0;
      i--){
    SCM new_len = scm_oneminus(len);
    len = new_len;
    SCM cur_item =
      scm_list_ref(states, new_len);

    SCM state_name_func =
      scm_c_public_ref("niebie game c-interface", "get-state-name");

    SCM cur_state_name =
      scm_call(state_name_func,
               cur_item,
               /* scm_from_int(i), */
               SCM_UNDEFINED);
    
    if(scm_is_string(cur_state_name)){
      char *str =
        scm_to_locale_string(cur_state_name);
      pdebug("what a state! (%s)",
             str);
      free(str);
    }
  }
  
  goto succ;

 succ:
 fail:
  return NULL;
}

struct screen_state_t default_states[] = {
  {
    "state 1", /* name */
    0, /* texture */
    0, /* actions */
    
    0, /* update */
    0, /* render */
    0, /* cleanup */
    0  /* initializer */
  },
  {
    "state 2", /* name */
    0, /* texture */
    0, /* actions */
    
    0, /* update */
    0, /* render */
    0, /* cleanup */
    0  /* initializer */
  },
  { 0 }
};

struct screen_resolution_t default_res = { 800, 600 };

struct config_t default_conf = {
  1,                 /* valid */
  1,                 /* fps_counter */
  default_states,    /* initial_state */
  default_states,    /* states */
  &default_res /* res */
};

void *load_config_file(void *args){
  struct config_t *result;
  if(!args){
    _perror("config file was null.");
    goto err;
  }
  
  SCM res =
    scm_c_primitive_load(root "states.scm");
  SCM res1 =
    scm_c_primitive_load(root "c-interface.scm");

 suc:
  return (void *)result;
 def:
  return (void *)&default_conf; 
 err:
  return NULL;
}

void read_configuration(char *config_file){
  if(!config_file){
    verbose_error_exit("config file not null assertion failed.");
  }

  /* use guile to load up the given config file */
  void *res =
    scm_with_guile(load_config_file,
                   (void *)config_file);

  /* we just know what load_config_file returns, so cast it direct. */
  struct config_t *res_config = (struct config_t *)res;
  if(!res_config){
    verbose_error_exit("res came back null");
  }else if(!res_config->valid){
    verbose_error_exit("res came back invalid");
  }else{
    pdebug("valid config loaded.");
    pdebug("** values **");
    pdebug(" - fps counter is %s",
           res_config->fps_counter ? "enabled" : "disabled");
    pdebug(" - initial state is '%s'",
           res_config->initial_state->name);
    pdebug(" - screen res is %d by %d",
           res_config->res->x, res_config->res->y);
  }
}

int main(int argc, char **argv){

  char *config;
  if(argc == 1){
    pdebug("no config file given - using default '"
           CONFIG_FILE
           "'");
    
    config = CONFIG_FILE;
  } else{
    config = argv[1];
    pdebug("config file given on command line - using '%s'",
           config);
  }
  
  read_configuration(config);
  
  return EXIT_SUCCESS;
}
