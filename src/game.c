#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/* local ../include/* */
#include <macros.h>
#include <game.h>
#include <screen_state.h>
#include <main_menu.h>

static struct game_t game;

static void init_sdl(){
  pdebug("start init sdl");
  TTF_Init();
  
  game.window = NULL;
  game.renderer = NULL;
  int posX = 100, posY = 100, width = 640, height = 480;
  game.window =
    SDL_CreateWindow("blade-of-the-sacred-wind",
                     posX,
                     posY,
                     width,
                     height,
                     0);
  game.renderer =
    SDL_CreateRenderer(game.window,
                       -1,
                       SDL_RENDERER_ACCELERATED);
  pdebug("end init sdl");
}

#define X(name, init) { name, NULL, NULL, NULL, NULL, NULL, init }
static struct screen_state_t states[] = {
  X("main menu", main_menu_init),
  { 0 }                         /* final field, checkable as null */
};
#undef X

static SDL_Surface *fps_surf;
static SDL_Texture *fps_text;
static SDL_Rect fps_pos = { 0, 0, 0, 0 };
static int frame_count = 0;
static clock_t start;
static clock_t stop;
static clock_t diff;

#define FIDELITY 1000
static void render(void){
  SDL_RenderClear(game.renderer);
  states[game.cur_state].render();

  /* pdebug("render"); */
  
  /* use the time between frames to get an fps. */
  /* compute every 1000th frame to be economical */
  if(!frame_count){
    // only start timer on 0.
    start = clock();
  }
  frame_count++;
  if(frame_count == FIDELITY){
    frame_count = 0;
    /* pdebug("getting time"); */
    stop = clock();
    diff = stop - start;
    /* pdebug("computing per frame [%d]", CLOCKS_PER_SEC); */
    double per_frame = ((double)diff / FIDELITY);
    /* pdebug("per_frame: [%f]", per_frame); */
    /* pdebug("frame time: [%f]", per_frame/ CLOCKS_PER_SEC); */
    int fps = (int)(CLOCKS_PER_SEC / per_frame);
    char fps_txt[10] = {0};
    sprintf(fps_txt, "%d", fps);

    if(fps_surf){
      SDL_FreeSurface(fps_surf);
    }
    if(fps_text){
      SDL_DestroyTexture(fps_text);
    }
    SDL_Color c = { 255, 255, 255};
    fps_surf =
      TTF_RenderText_Solid(game.font,
                           fps_txt,
                           c);
    fps_text =
      SDL_CreateTextureFromSurface(game.renderer,
                                   fps_surf);

    SDL_QueryTexture(fps_text,
                     NULL, NULL,
                     &fps_pos.w, &fps_pos.h);
    /* pdebug("fps: [%s]", fps_txt); */
    /* pdebug("fps pos: [%d, %d, %d, %d]", */
    /*        fps_pos.x, fps_pos.y, */
    /*        fps_pos.w, fps_pos.h); */
  }
  SDL_RenderCopy(game.renderer,
                 fps_text,
                 NULL,
                 &fps_pos);
  SDL_RenderPresent(game.renderer);
}

static void update(void){
  states[game.cur_state].update();
}

static void cleanup(void){
  pdebug("start cleanup");
  SDL_DestroyRenderer(game.renderer);
  SDL_DestroyWindow(game.window);
  pdebug("end cleanup");
}

static int check_quit(){
  /* SDL_Event e; */
  /* if (SDL_PollEvent(&e)) { */
  /*   if (e.type == SDL_QUIT) { */
  /*     return 1; */
  /*   } */
  /* } */
  return 0;
}

static void loop(void){
  while (1) {
    if(check_quit())
      return;
    update();
    render();
  }
}

static void assign_pointers(void){
  game.update = update;
  game.render = render;
  game.cleanup = cleanup;
  game.loop = loop;
}

static void init_states(void){
  game.cur_state = 0; // initial state
  game.state_count = 0;
  struct screen_state_t *tmp_state = &(states[0]);
  while(tmp_state && tmp_state->name){
    game.state_count++;
    tmp_state->initializer(tmp_state, game.renderer);
    tmp_state++;
  }
  game.states = states;
}

struct game_t *init_game(void){
  init_sdl();   
  assign_pointers();
  init_states();

  game.font = 
    TTF_OpenFont("/home/niebie/sc/telegram-build/tdesktop/Telegram/Resources/fonts/OpenSans-Regular.ttf",
                 13);
  
  if (game.font == NULL) {
    perror("error: font not found\n");
    printf("ttf failed: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  } 
  
  return &game;
}
