#include <libguile.h>
#include <stdlib.h>

/* local ../include/* */
#include <config.h>

#define CONFIG_FILE "./game-config.scm"
#define root "/home/niebie/sc/sdl/scm/modules/niebie/game/"
#define SCM_STR(str) (scm_from_locale_string(str))

static struct config_t config;

static void set_defaults(void){
  config.initial_state = NULL;
  config.states = NULL;
  config.res = { 800, 600 };
  config.fps_counter = 1;
}

static void *read_configuration(void *arg){
  char *config_file = (char *)arg;
  /* this is how we access records from c code. direct access with
     the getter functions defined in the srfi-9 records results in
     syntax errors that are not easy to track down, so we use the
     proxies that are defined in this shim. */
  scm_c_primitive_load(root "c-interface.scm");

  /* the arg we get is _supposed_ to be a char * that contains the
     config file name we need to load. */
  if(!config_file) {
    printf("no file given, using default as fallback ("
           CONFIG_FILE
           ")\n");
    config_file = CONFIG_FILE; 
  }

  /* expect an error here right now. */
  scm_c_primitive_load(config_file);
}

void load(void){
  set_defaults();

  /* override where needed. */
  void* res =
    scm_with_guile(read_configuration, NULL);
}
